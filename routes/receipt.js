var express = require('express');
var router = express.Router();

var Receipt = require('../models/receipt.js').Receipt;

router.post('/', function(req, res, next) {
  Receipt.create(req.body, function (err, post) {
    if (err) return next(err);
    res.json(post);
    console.log(req.body);
  });
});


module.exports = router;
