var express = require('express');
var router = express.Router();
var app = require('../app');
var bot = require('../helpers/telegram.js');

var Phone = require('../models/phone.js').Phone;

router.post('/', function (req, res, next) {

  console.log("Phone: " + req.body.phone);
  console.log("APP:" + app.io);

  Phone.find({
    phone: req.body.phone
  }, function (err, data) {
    if (data[0]) {
      //app.io.emit('message', "Постоянный посетитель. Предложить бонус.");
      if (bot.chatId)
        bot.sendMessage(bot.chatId, "Постоянный посетитель. Предложить бонус.");
    } else {
      if (bot.chatId)
        bot.sendMessage(bot.chatId, "Новый посетитель. Рассказать о системе лояльности.");
      //app.io.emit('message', "Новый посетитель. Рассказать о системе лояльности.");
    }

    Phone.create(req.body, function (err, post) {
      if (err) return next(err);
      res.json(post);
    });
  });
});

router.get('/getPhones', function (req, res, next) {
  if (!req.headers.hasOwnProperty('x-auth')) {
    return res.send(403);
  }
  if (req.headers['x-auth'] !== "foamily87764'hepatica") {
    return res.send(403);
  }

  return Phone.find({}, function (err, data) {
    if (err) return res.json(err);

    let refactoredList = [];

    data.forEach((visit, index, theArray) => {
      let item = {};
      item.phone = visit.phone;
      item.timestamp = visit.firstOccurance;
      item.id = visit._id;
      refactoredList.push(item);
    });

    return res.json(refactoredList);
  });
});

module.exports = router;
