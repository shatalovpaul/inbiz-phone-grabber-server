var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');

var app = express();
app.io = require('socket.io')();

module.exports = app;

var phone = require('./routes/phone');
var receipt = require('./routes/receipt');
var admin = require('./routes/admin');

var env = process.env.NODE_ENV || 'development';

app.locals.ENV = env;
app.locals.ENV_DEVELOPMENT = env == 'development';

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(favicon(__dirname + '/public/img/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/phone', phone);
app.use('/receipt', receipt);
app.use('/admin', admin);

/// catch 404 and forward to error handler
app.use(function (req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

/// error handlers

// development error handler
// will print stacktrace

if (app.get('env') === 'development') {
  app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err,
      title: 'error'
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {},
    title: 'error'
  });
});

app.io.on('connection', function (socket) {
  console.log('client connected');
});

mongoose.Promise = global.Promise;
// connect to MongoDB -
// TODO: move too settings file
mongoose.connect(env == 'development' ? 'mongodb://localhost:27017/olen' : 'mongodb://db:27017/olen').then(function () {
  console.log('Connection to ' + env + ' DB is succesful')
}).catch(function (err) {
  console.error(err)
});

