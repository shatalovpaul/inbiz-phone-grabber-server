
var mongoose = require('mongoose');

var Receipt = new mongoose.Schema({
  receipt: String,
  phone: String,
  updated: {
    type: Date,
    default: Date.now
  }
});

module.exports.Receipt = mongoose.model('Receipt', Receipt);
