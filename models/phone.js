
var mongoose = require('mongoose');

var Phone = new mongoose.Schema({
  phone: String,
  firstOccurance: {
    type: Date,
    default: Date.now
  }
});

module.exports.Phone = mongoose.model('Phone', Phone);
